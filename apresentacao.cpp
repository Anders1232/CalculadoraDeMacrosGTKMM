#include "apresentacao.hpp"


namespace CalculadoraDeMacros
{
	namespace GTKMM_Frontend
	{
		Apresentacao::Apresentacao(int argc, char** argv, LogicaNegocio::CalculadoraDeMacros* ln): ln(ln)
		{
			app=Gtk::Application::create (argc, argv, "org.anders1232.CalculadoraDeMacros.Gtk");
			builder=Gtk::Builder::create_from_file("/home/francisco/Git/CalculadoraDeMacros/JanelaPrincipal.glade");

			janelaPrincipal= Glib::RefPtr<Gtk::ApplicationWindow>::cast_dynamic(builder->get_object("JanelaPrincipal"));

			entryNome= Glib::RefPtr<Gtk::Entry>::cast_dynamic(builder->get_object("EntryNome"));
			spinButtonAltura= Glib::RefPtr<Gtk::SpinButton>::cast_dynamic(builder->get_object("SpinButtonAltura"));
			spinButtonIdade= Glib::RefPtr<Gtk::SpinButton>::cast_dynamic(builder->get_object("SpinButtonIdade"));
			spinButtonPeso= Glib::RefPtr<Gtk::SpinButton>::cast_dynamic(builder->get_object("SpinButtonPeso"));
			comboBoxTextAtividade= Glib::RefPtr<Gtk::ComboBoxText>::cast_dynamic(builder->get_object("ComboBoxTextAtividade"));
			comboBoxTextSexo= Glib::RefPtr<Gtk::ComboBoxText>::cast_dynamic(builder->get_object("ComboBoxTextSexo"));

			spinButtonMetaDeCalorias= Glib::RefPtr<Gtk::SpinButton>::cast_dynamic(builder->get_object("SpinButtonMetaDeCalorias"));
			spinButtonCarKg= Glib::RefPtr<Gtk::SpinButton>::cast_dynamic(builder->get_object("SpinButtonCarKg"));
			spinButtonProKg= Glib::RefPtr<Gtk::SpinButton>::cast_dynamic(builder->get_object("SpinButtonProKg"));
			spinButtonGorKg= Glib::RefPtr<Gtk::SpinButton>::cast_dynamic(builder->get_object("SpinButtonGorKg"));
			spinButtonCarDia= Glib::RefPtr<Gtk::SpinButton>::cast_dynamic(builder->get_object("SpinButtonCarDia"));
			spinButtonProDia= Glib::RefPtr<Gtk::SpinButton>::cast_dynamic(builder->get_object("SpinButtonProDia"));
			spinButtonGorDia= Glib::RefPtr<Gtk::SpinButton>::cast_dynamic(builder->get_object("SpinButtonGorDia"));
			labelMetabolismoBasal= Glib::RefPtr<Gtk::Label>::cast_dynamic(builder->get_object("LabelMetabolismoBasal"));
			labelEstimativaCalorica= Glib::RefPtr<Gtk::Label>::cast_dynamic(builder->get_object("LabelEstimativaCalorica"));
			labelCaloriasPlanejadas= Glib::RefPtr<Gtk::Label>::cast_dynamic(builder->get_object("LabelCaloriasPlanejadas"));

			ajusteCarKg= Glib::RefPtr<Gtk::Adjustment>::cast_dynamic(builder->get_object("AjusteCarKg"));
			ajusteProKg= Glib::RefPtr<Gtk::Adjustment>::cast_dynamic(builder->get_object("AjusteProKg"));
			ajusteGorKg= Glib::RefPtr<Gtk::Adjustment>::cast_dynamic(builder->get_object("AjusteGorKg"));
			ajusteCarDia= Glib::RefPtr<Gtk::Adjustment>::cast_dynamic(builder->get_object("AjusteCarDia"));
			ajusteProDia= Glib::RefPtr<Gtk::Adjustment>::cast_dynamic(builder->get_object("AjusteProDia"));
			ajusteGorDia= Glib::RefPtr<Gtk::Adjustment>::cast_dynamic(builder->get_object("AjusteGorDia"));

//			spinButtonAltura->signal_change().connect(sigc::mem_fun(this, &Apresentacao::CalcularMetabolismo));
			janelaPrincipal->set_title("Calculadora de Macros by Anders1232");


			Glib::RefPtr<Gtk::Adjustment>::cast_dynamic(builder->get_object("AjusteAltura"))->signal_value_changed().connect(sigc::mem_fun(this, &Apresentacao::CalcularMetabolismo));
			Glib::RefPtr<Gtk::Adjustment>::cast_dynamic(builder->get_object("AjusteIdade"))->signal_value_changed().connect(sigc::mem_fun(this, &Apresentacao::CalcularMetabolismo));
			Glib::RefPtr<Gtk::Adjustment>::cast_dynamic(builder->get_object("AjustePeso"))->signal_value_changed().connect(sigc::mem_fun(this, &Apresentacao::CalcularMetabolismo));
			comboBoxTextAtividade->signal_changed().connect(sigc::mem_fun(this, &Apresentacao::CalcularMetabolismo));
			comboBoxTextSexo->signal_changed().connect(sigc::mem_fun(this, &Apresentacao::CalcularMetabolismo));

			ajusteCarKg->signal_value_changed().connect(sigc::mem_fun(this, &Apresentacao::AtualizarMacrosPorDia) );
			ajusteProKg->signal_value_changed().connect(sigc::mem_fun(this, &Apresentacao::AtualizarMacrosPorDia) );
			ajusteGorKg->signal_value_changed().connect(sigc::mem_fun(this, &Apresentacao::AtualizarMacrosPorDia) );
			ajusteCarDia->signal_value_changed().connect(sigc::mem_fun(this, &Apresentacao::AtualizarMacrosPorKg) );
			ajusteProDia->signal_value_changed().connect(sigc::mem_fun(this, &Apresentacao::AtualizarMacrosPorKg) );
			ajusteGorDia->signal_value_changed().connect(sigc::mem_fun(this, &Apresentacao::AtualizarMacrosPorKg) );
		}
		Apresentacao::~Apresentacao(){}

		SEXO Apresentacao::DecodificarSexo(Glib::ustring id)
		{
			if("1" == id)
			{
				return SEXO::HOMEM;
			}
			if("2" == id)
			{
				return SEXO::MULHER;
			}
			else
			{
				return SEXO::NAO_DEFINIDO_SEXO;
//				throw new Glib::ustring("CalculadoraDeMacros::GTKMM_Frontend::Apresentacao::DecodificarSexo(Glib::ustring id) recebeu um id inválido" + id);
			}
		}

		NIVEL_ATIVIDADE Apresentacao::DecodificarAtividade(Glib::ustring id)
		{
			if("1" == id)
			{
				return NIVEL_ATIVIDADE::SEDENTARIO;
			}
			if("2" == id)
			{
				return NIVEL_ATIVIDADE::LEVEMENTE_ATIVO;
			}
			if("3" == id)
			{
				return NIVEL_ATIVIDADE::MODERADAMENTE_ATIVO;
			}
			if("4" == id)
			{
				return NIVEL_ATIVIDADE::ALTAMENTE_ATIVO;
			}
			if("5" == id)
			{
				return NIVEL_ATIVIDADE::EXTREMAMENTE_ATIVO;
			}
			return NIVEL_ATIVIDADE::NV_ATIVIDADE_NAO_DEFINIDO;
//			throw new Glib::ustring("CalculadoraDeMacros::GTKMM_Frontend::Apresentacao::DecodificarAtividade(Glib::ustring id) recebeu um id inválido" + id);
		}

		void Apresentacao::CalcularMetabolismo(void)
		{
			double basal= ln->MetabolismoBasal(
						DecodificarSexo(comboBoxTextSexo->get_active_id()),
						spinButtonAltura->get_value_as_int(),
						spinButtonIdade->get_value_as_int(),
						static_cast<double>(spinButtonPeso->get_value())
						);
			double estimaticaCalorica= ln->EstimativaCalorica(basal, DecodificarAtividade(comboBoxTextAtividade->get_active_id()));
			labelMetabolismoBasal->set_text(std::to_string(basal));
			labelEstimativaCalorica->set_text(std::to_string(estimaticaCalorica));
		}
		void Apresentacao::Exibir(int argc, char**argv)
		{
			app->run( *(janelaPrincipal.get()), argc, argv);
		}

		void Apresentacao::AtualizarCaloriasPlanejadas(void)
		{
			labelCaloriasPlanejadas->set_text(std::to_string(ln->CaloriasPlanejadas(
												  spinButtonCarDia->get_value(),
												  spinButtonProDia->get_value(),
												  spinButtonGorDia->get_value()
																 )
															 )
											  );
		}

		void Apresentacao::AtualizarMacrosPorDia(void)
		{
			ajusteCarDia->set_value(
						ln->CarPorKgPraCarPorDia(
							spinButtonPeso->get_value(),
							spinButtonCarKg->get_value()
							)
						);


			ajusteProDia->set_value(
						ln->ProPorKgPraProPorDia(
							spinButtonPeso->get_value(),
							spinButtonProKg->get_value()
							)
						);


			ajusteGorDia->set_value(
						ln->GorPorKgPraGorPorDia(
							spinButtonPeso->get_value(),
							spinButtonGorKg->get_value()
							)
						);
			AtualizarCaloriasPlanejadas();
		}

		void Apresentacao::AtualizarMacrosPorKg(void)
		{
			ajusteCarKg->set_value(
						ln->CarPorDiaPraCarPorKg(
							spinButtonPeso->get_value(),
							spinButtonCarDia->get_value()
							)
						);


			ajusteProKg->set_value(
						ln->ProPorDiaPraProPorKg(
							spinButtonPeso->get_value(),
							spinButtonProDia->get_value()
							)
						);


			ajusteGorKg->set_value(
						ln->GorPorDiaPraGorPorKg(
							spinButtonPeso->get_value(),
							spinButtonGorDia->get_value()
							)
						);
			AtualizarCaloriasPlanejadas();
		}
	}
}

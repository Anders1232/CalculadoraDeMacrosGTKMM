#ifndef TIPOSBASICOS_HPP
#define TIPOSBASICOS_HPP

#include <stdint.h>
#include <vector>
#include <glibmm/ustring.h>

typedef struct tm tm;

namespace CalculadoraDeMacros
{
	enum SEXO
	{
		NAO_DEFINIDO_SEXO=0,
		HOMEM,
		MULHER,
		SIZE_SEXO
	};
	enum NIVEL_ATIVIDADE
	{
		NV_ATIVIDADE_NAO_DEFINIDO=0,
		SEDENTARIO,
		LEVEMENTE_ATIVO,
		MODERADAMENTE_ATIVO,
		ALTAMENTE_ATIVO,
		EXTREMAMENTE_ATIVO,
		SIZE_NV_ATIVIDADE
	};
	class Data
	{
		public:
			Data(int diaDoMes, int mesDoAno, int ano);
			Data(tm data);
			~Data();
			int diaDoMes;
			int mesDoAno;
			int ano;
	};
	struct Hora
	{
		public:
			Hora(void);
			int hora;
			int minutos;
	};
	struct Usuario
	{
		public:
			int64_t ID;
			Glib::ustring Nome;
			Data dataNascimento;
			SEXO sexo;
	};
	struct DadosUsuario
	{
		public:
			int64_t Id;
			int64_t IdUsuario;
			Data data;
			int alturaEmCm;
			double peso;
			NIVEL_ATIVIDADE nivelAtividade;
	};
	struct DadosMetaCalorica
	{
		public:
			int64_t id;
			int64_t idDadosUsuario;
			int metaCalorica;
			int carKg;
			int proKg;
			int gorKg;
	};
	struct Alimento
	{
		public:
			int ObterCarPorcao(int porcaoEmGramas);
			int ObterProPorcao(int porcaoEmGramas);
			int ObterGorPorcao(int porcaoEmGramas);
			int64_t id;
			Glib::ustring nomeAlimento;
			int pesoPorcaoBase;
			int carProcaoBase;
			int proPorcaoBase;
			int GorProcaoBase;
	};
	struct Dieta
	{
		public:
			int64_t id;
			int64_t idUsuario;
			int64_t idMetaCalorica;
			Data data;
			Hora hora;
			int64_t idAlimento;
			int pesoAlimento;
	};
	class DadosExecução
	{
		std::vector<Usuario> usuarios;
		std::vector<DadosUsuario> dadosUsuarios;
		std::vector<DadosMetaCalorica> dadosMetaCalorica;
		std::vector<Alimento> alimentos;
		std::vector<Dieta> dieta;
	};
}

#endif // TIPOSBASICOS_HPP

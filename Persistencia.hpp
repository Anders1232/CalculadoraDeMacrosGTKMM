#ifndef PERSISTENCIA_HPP
#define PERSISTENCIA_HPP

#include <memory>
#include <sqlite/connection.hpp>
#include <sqlite/execute.hpp>
#include <sqlite/query.hpp>

class Persistencia
{
	public:
		Persistencia();
		~Persistencia();
		CarregarArquivo(std::string caminho);
};

#endif // PERSISTENCIA_HPP

#include <string>
#include "CalculadoraDeMacros.hpp"

namespace CalculadoraDeMacros
{
	namespace LogicaNegocio
	{
		CalculadoraDeMacros::CalculadoraDeMacros(){};
		CalculadoraDeMacros::~CalculadoraDeMacros(){};

		double CalculadoraDeMacros::MetabolismoBasal(SEXO sexo, int altura, int idade, double peso)
		{
			if(SEXO::HOMEM == sexo)
			{
				return 66.+13.7*peso + 5*altura - 6.8*static_cast<double>(idade);
			}
			else if (SEXO::MULHER == sexo)
			{
				return 655.+9.6*peso+1.8*altura - 4.7*static_cast<double>(idade);
			}
			else
			{
				return -1.;
//				throw new std::string("[ERRO]LogicaNegocio::CalculadoraDeMacros::MetabolismoBasal recebeu sexo inválido");
			}
		}

		double CalculadoraDeMacros::EstimativaCalorica(double metabolismoBasal, NIVEL_ATIVIDADE nvAtividade)
		{
			if(NIVEL_ATIVIDADE::SEDENTARIO == nvAtividade)
			{
				return metabolismoBasal*1.2;
			}
			else if(NIVEL_ATIVIDADE::LEVEMENTE_ATIVO == nvAtividade)
			{
				return metabolismoBasal*1.375;
			}
			else if(NIVEL_ATIVIDADE::MODERADAMENTE_ATIVO == nvAtividade)
			{
				return metabolismoBasal*1.55;
			}
			else if(NIVEL_ATIVIDADE::ALTAMENTE_ATIVO == nvAtividade)
			{
				return metabolismoBasal*1.725;
			}
			else if(NIVEL_ATIVIDADE::EXTREMAMENTE_ATIVO == nvAtividade)
			{
				return metabolismoBasal*1.9;
			}
			else
			{
				return -1.;
				//				throw new std::string("[ERRO]LogicaNegocio::CalculadoraDeMacros::EstimativaCalorica recebeu nível de atividade inválido");
			}
		}

		double CalculadoraDeMacros::CaloriasPlanejadas(double carbo, double prote, double gord)
		{
			return carbo*4+prote*4+gord*9;
		}

		double CalculadoraDeMacros::CaloriasPlanejadas(double peso, double carboPorKg, double protePorKg, double gordPorKg)
		{
			return CaloriasPlanejadas(carboPorKg*peso, protePorKg*peso, gordPorKg*peso);
		}

		double CalculadoraDeMacros::CarPorKgPraCarPorDia(double peso, double carbPorKg)
		{
			return peso*carbPorKg;
		}
		double CalculadoraDeMacros::CarPorDiaPraCarPorKg(double peso, double carbTotal)
		{
			return carbTotal/peso;
		}

		double CalculadoraDeMacros::ProPorKgPraProPorDia(double peso, double proPorKg)
		{
			return peso*proPorKg;
		}
		double CalculadoraDeMacros::ProPorDiaPraProPorKg(double peso, double proTotal)
		{
			return proTotal/peso;
		}

		double CalculadoraDeMacros::GorPorKgPraGorPorDia(double peso, double gorPorKg)
		{
			return peso*gorPorKg;
		}
		double CalculadoraDeMacros::GorPorDiaPraGorPorKg(double peso, double gorTotal)
		{
			return gorTotal/peso;
		}
	}
}

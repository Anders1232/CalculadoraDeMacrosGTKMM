#ifndef APRESENTACAO_HPP
#define APRESENTACAO_HPP

#include <gtkmm-3.0/gtkmm.h>
#include "CalculadoraDeMacros.hpp"


namespace CalculadoraDeMacros
{
	namespace GTKMM_Frontend
	{
		class Apresentacao
		{
			public:
				Apresentacao(int argc, char**argv, LogicaNegocio::CalculadoraDeMacros* ln);
				~Apresentacao();
				void Exibir(int argc, char**argv);
//				void AtribuirLogicaNegocio(LogicaNegocio::CalculadoraDeMacros* ln);
//				Glib::RefPtr<Gtk::Window> MainWindow(void);
			private:
				void CalcularMetabolismo(void);
				SEXO DecodificarSexo(Glib::ustring id);
				NIVEL_ATIVIDADE DecodificarAtividade(Glib::ustring id);
				void AtualizarMacrosPorDia(void);
				void AtualizarMacrosPorKg(void);
				void AtualizarCaloriasPlanejadas(void);

				LogicaNegocio::CalculadoraDeMacros* ln;

/*				Gtk::Box& abaPessoa;
				Gtk::Box& abaAlimentos;
				Gtk::Box& abaDieta;
*/
				Glib::RefPtr<Gtk::Application> app;
				Glib::RefPtr<Gtk::Builder> builder;

				Glib::RefPtr<Gtk::ApplicationWindow> janelaPrincipal;

				Glib::RefPtr<Gtk::Entry> entryNome;
				Glib::RefPtr<Gtk::SpinButton> spinButtonAltura;
				Glib::RefPtr<Gtk::SpinButton> spinButtonIdade;
				Glib::RefPtr<Gtk::SpinButton> spinButtonPeso;
				Glib::RefPtr<Gtk::ComboBoxText> comboBoxTextAtividade;
				Glib::RefPtr<Gtk::ComboBoxText> comboBoxTextSexo;

				Glib::RefPtr<Gtk::SpinButton> spinButtonMetaDeCalorias;
				Glib::RefPtr<Gtk::SpinButton> spinButtonCarKg;
				Glib::RefPtr<Gtk::SpinButton> spinButtonProKg;
				Glib::RefPtr<Gtk::SpinButton> spinButtonGorKg;
				Glib::RefPtr<Gtk::SpinButton> spinButtonCarDia;
				Glib::RefPtr<Gtk::SpinButton> spinButtonProDia;
				Glib::RefPtr<Gtk::SpinButton> spinButtonGorDia;
				Glib::RefPtr<Gtk::Label> labelMetabolismoBasal;
				Glib::RefPtr<Gtk::Label> labelEstimativaCalorica;
				Glib::RefPtr<Gtk::Label> labelCaloriasPlanejadas;

				Glib::RefPtr<Gtk::Adjustment> ajusteCarKg;
				Glib::RefPtr<Gtk::Adjustment> ajusteProKg;
				Glib::RefPtr<Gtk::Adjustment> ajusteGorKg;
				Glib::RefPtr<Gtk::Adjustment> ajusteCarDia;
				Glib::RefPtr<Gtk::Adjustment> ajusteProDia;
				Glib::RefPtr<Gtk::Adjustment> ajusteGorDia;
		};
	}
}
#endif // APRESENTACAO_HPP

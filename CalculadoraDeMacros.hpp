#ifndef CALCULADORADEMACROS_HPP
#define CALCULADORADEMACROS_HPP

#include <memory>
#include "TiposBasicos.hpp"

namespace CalculadoraDeMacros
{
	namespace LogicaNegocio
	{
		class CalculadoraDeMacros
		{
			public:
				CalculadoraDeMacros();
				~CalculadoraDeMacros();

				double MetabolismoBasal(SEXO sexo, int altura, int idade, double peso);
				double EstimativaCalorica(double metabolismoBasal, NIVEL_ATIVIDADE nvAtividade);

				double CaloriasPlanejadas(double peso, double carboPorKg, double protePorKg, double gordPorKg);
				double CaloriasPlanejadas(double carbo, double prote, double gord);

				double CarPorKgPraCarPorDia(double peso, double carbPorKg);
				double CarPorDiaPraCarPorKg(double peso, double carbTotal);

				double ProPorKgPraProPorDia(double peso, double proPorKg);
				double ProPorDiaPraProPorKg(double peso, double proTotal);

				double GorPorKgPraGorPorDia(double peso, double gorPorKg);
				double GorPorDiaPraGorPorKg(double peso, double gorTotal);

			private:
				std::shared_ptr<DadosExecução> dados;
		};
	}
}
#endif // CALCULADORADEMACROS_HPP

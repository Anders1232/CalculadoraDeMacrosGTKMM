#include <gtkmm-3.0/gtkmm.h>
#include <iostream>

#include "apresentacao.hpp"
#include "CalculadoraDeMacros.hpp"

int main(int argc, char** argv)
{
	try{
		//Persistencia persistencia;
		CalculadoraDeMacros::LogicaNegocio::CalculadoraDeMacros logicaDeNegocio;
		CalculadoraDeMacros::GTKMM_Frontend::Apresentacao apresentacao(argc, argv, &logicaDeNegocio);
		apresentacao.Exibir(argc, argv);
	}
	catch(Glib::FileError& error)
	{
		std::cout<< error.what() << std::endl;
	}
	return 0;
}
